# Revision history for SesParser

## 0.2.0.0 -- 2019-01-23

### Breaking Changes

The only breaking changes should be on the library side.  In case
someone started using this library in the past day.

- Data.Ses.Parser now exports a parser from the Megaparsec module,
  instead of the old Attoparsec module.

- Data.Ses.Types no longer exports Arbitrary instances for the types.
  This prevents the type system from being polluted by Arbitrary Text,
  which should only be needed for testing.

- The build system has been tightened so that all compiler warnings
  are now errors.  This has already caught several bugs.


### New Functionality

- The `down-convert` option has been added for turning spin echo files
  written according to the spec into the old generation of file
  format.  As the original spec wasn't strictly defined, this is a
  best effort.  Bug reports are appreciated if applications won't load
  the down converted code.

- If sestool cannot read a file, it now states the exactly line and
  column in the file where the failure occurred and what values it was
  expecting at this point.  This should help anyone manually editing
  files.

## 0.1.0.0  -- 2019-01-22

* First version. Released on an unsuspecting world.
