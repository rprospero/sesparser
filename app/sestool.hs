{-# LANGUAGE FlexibleContexts, ScopedTypeVariables, RankNTypes, OverloadedStrings #-}
import Control.Arrow (left)
import Control.Exception (try)
import Control.Lens hiding (argument)
import Control.Monad (forM_)
import Control.Monad.State.Strict
import Control.Monad.Trans.Except
import Data.List (foldl1', foldl')
import Data.Ses
import Data.Ses.Writer (printSes, printSesOld)
import Data.Text (Text, unpack, pack)
import qualified Data.Text.IO as T
import Options.Applicative
import System.IO (stderr, hPutStrLn)
import Text.Megaparsec (parse, parseErrorPretty)

data Command = Combine
  | DownConvert
  | Rescale Double [Text]
  | Filter Text Double Double
  deriving (Show)

data AllOptions = AllOptions {
  allOptionsOutput :: (Maybe FilePath)
  , allOptionsCommand :: Command
  , allOptionsFiles :: [FilePath]
}

safeRead :: FilePath -> IO (Either IOError Text)
safeRead = try . T.readFile

runCmd :: SesData -> Command -> Either String Text
runCmd _ Combine = Left "This code should be unreachable"
runCmd f DownConvert = Right . printSesOld $ f
runCmd f (Rescale scale cols) = do
  let go s c = s & colLens c %~ rescaleColumn scale
  let fixed = foldl' go f cols
  Right $ printSes fixed
runCmd dat (Filter column low high) =
  let base = note ("Column " <> column <> "missing") $ dat ^? colLens column . _sesColValues :: Either Text [Double]
      mask = map (\x -> (x >= low) && (x <= high)) <$> base
      runMask m = do
        _sesEchoLength %= filterColumn m
        _sesDepolarisation %= filterColumn m
        _sesWavelength %= filterColumn m
        _sesColumns . traverse %= filterColumn m
      new = flip execState dat . runMask <$> mask
  in
    left unpack (printSes <$> new)

filterColumn :: [Bool] -> SesColumn -> SesColumn
filterColumn mask = execState $ do
  _sesColValues %= map fst . filter snd . flip zip mask
  _sesColError . _Just %= map fst . filter snd . flip zip mask

note :: a -> Maybe b -> Either a b
note a Nothing = Left a
note _ (Just b) = Right b

colLens :: Applicative f => Text -> (SesColumn -> f SesColumn) -> SesData -> f SesData
colLens "SpinEchoLength" =  _sesEchoLength . _Identity
colLens "Depolarisation" =  _sesDepolarisation . _Identity
colLens "Wavelength" =  _sesWavelength . _Identity
colLens  c = _sesColumns . at c . _Just

_Identity :: Prism' a a
_Identity = prism' id Just

rescaleColumn :: Double -> SesColumn -> SesColumn
rescaleColumn s = (_sesColValues . each *~ s) . (_sesColError . _Just . each*~ s)


errorFix :: FilePath -> IOError -> String
errorFix f x = "Error in file " <> show f <> ":\n" <> show x

safeSES :: FilePath -> IO (Either String SesData)
safeSES f = runExceptT $ do
  dat <- ExceptT (left (errorFix f) <$> safeRead f)
  ExceptT . return . left parseErrorPretty $ parse ses f dat


main :: IO ()
main = do
  options <- execParser opts
  final <- runExceptT $ do
    fileData <- ExceptT . liftM sequence $ mapM safeSES $ allOptionsFiles options :: ExceptT String IO [SesData]
    -- ExceptT . return $ (flip runCmd $ allOptionsCommand options) fileData
    case allOptionsCommand options of
      Combine -> return . printSes $ foldl1' (<>) fileData
      x -> case fileData of
        [] -> throwE "No files given"
        [f] -> ExceptT . return $ runCmd f x
        _ -> do
          forM_ (zip (allOptionsFiles options) fileData) $ \(f, d) -> do
            result <- ExceptT . return $ runCmd d x
            lift $ T.writeFile f result
          return ""
  case final of
    Left err -> hPutStrLn stderr err
    Right x -> case allOptionsOutput options of
      Nothing -> T.putStrLn x
      Just f -> T.writeFile f x
  where
    allOpts :: Parser AllOptions
    allOpts = AllOptions
      <$> optional (option str $ long "output"
                    <> help "A file where the result will be saved.  By default, display on the screen."
                    <> short 'o'
                    <> metavar "OUTFILE")
      <*> cmd
      <*> some (argument str $ metavar "FILES")
    cmd :: Parser Command
    cmd = subparser (
      command "combine" (info combine (progDesc "Combine multiple files into single SES file."))
      <> command "down-convert" (info downConvert (progDesc "Convert files into the old format"))
      <> command "rescale" (info rescale (progDesc "Adjust the columns by a factor"))
      <> command "filter" (info filterCol (progDesc "Filter the data points based on one column"))
      )
    combine :: Parser Command
    combine = pure Combine
    downConvert :: Parser Command
    downConvert = pure DownConvert
    filterCol :: Parser Command
    filterCol = Filter
      <$> (pack <$> argument str (help "The on which to filter the data"
                        <> metavar "COLUMN"))
      <*> (option auto ( long "low"
                            <> short 'l'
                            <> metavar "VAL"
                            <> value (read "-Infinity")
                            <> help "The minimum column value to accept"))
      <*> (option auto ( long "high"
                            <> short 'h'
                            <> metavar "VAL"
                            <> value (read "Infinity")
                            <> help "The maximum column value to accept"))
    rescale :: Parser Command
    rescale = Rescale
      <$> argument auto (help "A multiplicative factor to scale the column (and its error) by"
                        <> metavar "SCALE")
      <*> some (option str ( long "column"
                            <> short 'c'
                            <> metavar "COL"
                            <> help "Column name to change"))
    opts = info (allOpts <**> helper) $ fullDesc <> progDesc "A general utility for handling spin echo SES files.  Comands include: combine, down-convert, filter, and scale"
