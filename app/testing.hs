{-# LANGUAGE OverloadedStrings #-}
import Control.Lens hiding (elements)
import Control.Monad ((>=>))
import qualified Data.Map.Strict as M
import Data.Map.Strict (fromList)
import Data.Function (on)
import Data.List (nubBy, sort)
import Data.Maybe (isNothing)
import Data.Monoid ((<>))
import Data.Text (Text, pack)
import Data.Text.IO as T
import Data.Void
import Test.QuickCheck.Instances.Text()
import Test.Tasty
import Test.Tasty.HUnit
import Test.Tasty.QuickCheck
import Text.Megaparsec

import Data.Ses
import Data.Ses.Writer (printSes)

instance Arbitrary SesData where
  arbitrary = (SesData 1.0
    <$> simpleMap
    <*> elements "YZ"
    <*> arbitrary
    <*> arbitrary
    <*> arbitrary
    <*> colWithAllSorted
    <*> colWithAll
    <*> colWithAll
    <*> simpleMap)
    `suchThat`
    (\x -> not . any (`M.member` (x ^. _sesColumns)) . M.keys $ x ^. _sesHeader)

simpleText :: Gen Text
simpleText = pack <$> listOf1 (elements $ ['a'..'z'] <> ['A'..'Z'])

simpleMap :: Arbitrary a => Gen (M.Map Text a)
simpleMap = do
  ixes <- listOf1 simpleText
  values <- sequence $ repeat arbitrary
  return . M.fromList . zip ixes $ values

instance Arbitrary HeaderTerm where
  arbitrary = oneof [Raw <$> simpleText , Measure <$> arbitrary]
  shrink = genericShrink

instance Arbitrary Measurement where
  arbitrary = Measurement <$> arbitrary <*> arbitrary
  shrink = genericShrink

instance Arbitrary SesColumn where
  arbitrary = SesColumn <$> arbitrary <*> (Just <$> vectorOf 5 arbitrary) <*> vectorOf 5 arbitrary
  shrink = genericShrink

colWithAll :: Gen SesColumn
colWithAll = SesColumn <$> (Just <$> arbitrary) <*> (Just <$> vectorOf 5 arbitrary) <*> vectorOf 5 arbitrary

colWithAllSorted :: Gen SesColumn
colWithAllSorted = SesColumn <$> (Just <$> arbitrary) <*> (Just <$> vectorOf 5 arbitrary) <*> (sort <$> vectorOf 5 arbitrary)

instance Arbitrary Units where
  arbitrary = Units . nubBy (on (==) fst) <$> listOf1 arbUnit
  shrink = genericShrink

arbUnit :: Gen (Text, Int)
arbUnit = (,) <$> (pack <$> elements ["cm", "A", "radians"]) <*> (arbitrary `suchThat` (/= 0))


------------

badExample =
      SesData {sesVersion = 1.0, sesHeader = fromList [("erodcpdyjxs",Measure (Measurement (-5.035788008011169) (Units [("radians",-1),("cm",4),("A",-11)]))),("ffhcuofb",Measure (Measurement (-1.3290919039427016) (Units [("cm",8)]))),("h",Measure (Measurement (-12.055382056640363) (Units [("cm",2),("A",8),("radians",-9)]))),("hovagkqaxy",Measure (Measurement (-0.3714238321811776) (Units [("cm",6)]))),("ipd",Raw "b"),("jinx",Measure (Measurement (-44.59070126398882) (Units [("cm",10),("A",-9)]))),("k",Measure (Measurement (-10.368017147222012) (Units [("cm",8),("radians",11),("A",6)]))),("mhblq",Raw "igl"),("mzhdamvhlcc",Raw "gbb"),("qm",Raw "f"),("vjq",Raw "z")], sesOrientation = 'Z', sesThickness = Measurement (-2.196799555879541) (Units [("cm",10),("A",-10),("radians",3)]), sesThetaY = Measurement 11.949945418934492 (Units [("A",-8),("cm",-1),("radians",5)]), sesThetaZ = Measurement 139.88896617505898 (Units [("A",6),("cm",1),("radians",9)]), sesEchoLength = SesColumn {sesColUnit = Just (Units [("A",5),("cm",8),("radians",-8)]), sesColError = Just [-1.8860900506186227,27.923065789653933,16.91903631133375,-8.468028834277844,-3.21019137990522], sesColValues = [-0.4653297548715685,-3.2230469893436133,-27.788991667017935,17.632323006098655,568.805990106592]}, sesDepolarisation = SesColumn {sesColUnit = Just (Units [("cm",-6),("A",-2),("radians",-11)]), sesColError = Just [623.252882572834,25.55616696102668,10.355650857319077,-14.519430523065978,-1.8836789146815882], sesColValues = [-15.608663431483164,-66.66616091541506,-21.226753378940778,-9.173665170841117,9.773694798016402]}, sesWavelength = SesColumn {sesColUnit = Just (Units [("radians",-7),("A",0)]), sesColError = Just [77.58480252969093,-4.846476453771868,53.51183015433881,0.4853765358138862,6.977832263607517], sesColValues = [-25.67633467739443,6.401725209324107,-207.33228510029053,-7.887947242688961,19.001926076211547]}, sesColumns = fromList [("arycwrmehf",SesColumn {sesColUnit = Just (Units [("cm",7),("A",-6)]), sesColError = Just [1.1289430317660931,-38.279537759436714,12.763685585996912,2.6297166970222623,7.975500506541608], sesColValues = [-9.397301959944201,-5.254618746367665,-12.718818296590872,7.9245495894754026,-20.921195556902447]}),("cxvy",SesColumn {sesColUnit = Just (Units [("cm",8),("radians",-4)]), sesColError = Just [7.560341731456321,3.6276982687573556,-9.502830726206184,-11.739429602680351,9.733256723372902], sesColValues = [-13.170280359340667,-7.155610666734349,-196.15943514690585,34.856913505957095,1.3538081668526996]}),("gsvdkfckais",SesColumn {sesColUnit = Just (Units [("A",5),("cm",0)]), sesColError = Just [6.654613140969573,-4.816456825075229,24.73235796784245,12.824485537203078,-158.45383907433026], sesColValues = [570.1512148341997,-15.905674670007485,13.994655560100233,29.41147821674111,9.679239339121333]}),("hwunrt",SesColumn {sesColUnit = Just (Units [("radians",10),("A",-11),("cm",1)]), sesColError = Just [13.907565236482936,4.874125143605892,-3.180419761208537,-21.68058520663565,3.055880089044846], sesColValues = [14.386969101630438,16.591161316191787,49.98045006472298,328.6649450556996,-1.4852420908177602]}),("k",SesColumn {sesColUnit = Just (Units [("cm",9),("radians",1),("A",2)]), sesColError = Just [31.570663916644808,4.184408459643358,-10.7806236140955,-15.43430017636193,19.32081494695376], sesColValues = [-45.23360194647165,10.705255259340083,56.4127679048807,-15.101169914930667,-18.910963902521917]}),("lueb",SesColumn {sesColUnit = Nothing, sesColError = Just [-14.981272238350938,5.666552935674342,10.177792745463188,8.817707742514612,-73.1260184461935], sesColValues = [-5.216122769381587,9.062734753153752,71.2220431413549,-6.750753382580974,-7.1794783277819905]}),("mnhywqu",SesColumn {sesColUnit = Just (Units [("radians",-8)]), sesColError = Just [15.113325281767857,52.50640374205384,-16.872188727972965,-4.401521482107391,20.513034911129246], sesColValues = [-86.4229107054299,-114.52102958651761,9.420044036970845,3.235747925968264,-4.502394386796837]}),("naa",SesColumn {sesColUnit = Nothing, sesColError = Just [-20.280272427860048,-6.453429529644336,-7.20152301890721,-10.391791940018354,39.82213149057679], sesColValues = [18.023824610109088,-12.501095427924659,-9.685007518255581,5.438609888803755,3.2666587177100297]}),("sjvaktd",SesColumn {sesColUnit = Nothing, sesColError = Just [-12.619094768689804,-80.21580704327901,-3.294759826648819,23.287967068413103,2.3142104931053034], sesColValues = [-18.64258699991759,44.79653427463922,40.60213493634843,9.676403439471096,-1.3447253299998545]}),("v",SesColumn {sesColUnit = Nothing, sesColError = Just [15.815169053154344,7.150319005041332,-5.739328177728275,-208.04245832784704,24.242614786133295], sesColValues = [-14.332959807937467,4.533957886993743,2.4920598525469586,-19.95450067674432,9.062697594940227]})]}

readSesansFile :: FilePath -> IO (Either (ParseError (Token Text) Void) SesData)
readSesansFile = (T.readFile . ("test/" <>)) >=> (return . parse ses "")

-- main :: IO ()
-- main = do

main :: IO ()
main = defaultMain tests

tests :: TestTree
tests = testGroup "Tests" [sphere2micron
                          , sesansTof
                          , test_writer
                          , test_real
                          , badFiles]

sphere2micron :: TestTree
sphere2micron = testGroup "Sphere2micron.ses"
  [ testCase "Low Spin Echo Length" $ do
      Right example <- readSesansFile "sphere2micron.ses"
      391.56 @=? (head . sesColValues . sesEchoLength$ example)
  , testCase "High Spin Echo Length" $ do
      Right example <- readSesansFile "sphere2micron.ses"
      46099 @=? (last . sesColValues . sesEchoLength$ example)
  , testCase "High Spin Depolarisation" $ do
      Right example <- readSesansFile "sphere2micron.ses"
      -0.19956 @=? (last . sesColValues . sesDepolarisation$ example)
  , testCase "Spin Echo Unit" $ do
      Right example <- readSesansFile "sphere2micron.ses"
      Just (Units [("A", 1)]) @=? (sesColUnit . sesEchoLength$ example)
  , testCase "Depolarisation Unit" $ do
      Right example <- readSesansFile "sphere2micron.ses"
      Just (Units [("A", -2), ("cm", -1)]) @=? (sesColUnit . sesDepolarisation$ example)
  , testCase "Sample Name" $ do
      Right example <- readSesansFile "sphere2micron.ses"
      Just (Raw "Polystyrene 2 um in 53% H2O, 47% D2O")  @=? (M.lookup "Sample" . sesHeader$ example)
  , testCase "Thickness" $ do
      Right example <- readSesansFile "sphere2micron.ses"
      Measurement 0.2 (Units [("cm", 1)]) @=? sesThickness example
  , testCase "Z Acceptance" $ do
      Right example <- readSesansFile "sphere2micron.ses"
      Measurement 0.0168 (Units [("radians", 1)]) @=? sesThetaZ example
    ]

sesansTof :: TestTree
sesansTof = testGroup "sphere_isis.ses"
  [ testCase "Loading Data" $ do
      Right example <- readSesansFile "sphere_isis.ses"
      19303.4 @=? (last . sesColValues . sesEchoLength$ example)
      13.893668 @=? (last . sesColValues . sesWavelength$ example)
      1.612452 @=? (head . sesColValues . sesWavelength$ example)
      Measurement 2 (Units [("mm", 1)]) @=? sesThickness example
      Measurement 0.09 (Units [("radians", 1)]) @=? sesThetaZ example]

test_writer :: TestTree
test_writer = testGroup "Testing file writer"
  [ testCase "round trip writer" $ do
      Right example <- readSesansFile "sphere_isis.ses"
      Right example @=? parse ses "" (printSes example)
  -- , testCase "bad example print" $
  --   "" @=? (printSes badExample)
  -- , testCase "bad example" $ Right badExample @=? parseOnly ses (printSes badExample)
  , testProperty "round trip quickcheck" $ \x ->
      isNothing (x ^. _sesHeader . at "") && isNothing (x ^. _sesColumns . at "")  ==> Right x === parse ses "" (printSes x)]

test_real :: TestTree
test_real = testGroup "Testing real data"
  [ testCase "34909_semsans" $ do
      example <- readSesansFile "34909_semsans.ses"
      M.lookup "Sample" . sesHeader <$> example @?= Right (Just (Raw "34909_semsans"))
  , testCase "34911_semsans" $ do
      example <- readSesansFile "34911_semsans.ses"
      M.lookup "Sample" . sesHeader <$> example @?= Right (Just (Raw "34911_semsans"))
      return ()]

badFiles :: TestTree
badFiles = testGroup "Testing malformed files"
  [ testCase "no_spin_echo_unit" $ do
      example <- readSesansFile "no_spin_echo_unit.ses"
      parseErrorPretty <$> (example ^? _Left) @?= Just "73:1:\nNo units for column SpinEchoLength\n"
  -- , testCase "no_wavelength" $ do
  --     example <- readSesansFile "no_wavelength.ses"
  --     example @?= Left "Failed reading: Unequal number of headers and columns"
  , testCase "too_many_headers" $ do
      example <- readSesansFile "too_many_headers.ses"
      parseErrorPretty <$> (example ^? _Left) @?= Just "74:1:\nUnequal number of headers and columns\n"
  , testCase "next_gen" $ do
      example <- readSesansFile "next_gen.ses"
      parseErrorPretty <$> (example ^? _Left) @?= Just "2:1:\nVersion 2.0 not yet supported\n"]
