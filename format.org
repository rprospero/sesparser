* Overview

The SES file format is a text based format for recording SESANS data.
The file consists of two parts.  The first part is a header,
containing the metadata for the file.  Values such as sample
thickness, measurement units, and sample names are stored in this
header.  The remainder of the file is dedicated to the actual neutron
data.

The SES file is a text based format that, while intended to be mainly
read and written by software, should still be trivially parsed and
modified by a human user with a simple text editor.  UTF-8 is
recommended for the actual text encoding, but any programs should be
ready to accept other encoding schemes.

* Header

The header is a key-value store.  Each key value pair will be on its
own line.  The line will begin with the key, followed by white space,
followed by the value.  The value may contain white space, but may not
contain a new line character.  The key cannot contain any white space.
Certain classes of defined parameters have constrained parsing
grammars.  The order of the keys is not significant, with the
exception of the =FileFormatVersion=, which *MUST* be the first line
of the file.

** Orientation

   Values of the =Orientation= parameter can only ever be =Y= or =Z=.

** FileFormatVersion

   The value of =FileFormatVersion= is a version string.  This
   document describes version 1.0 of the file format.  The versions
   will follow the Semantic Versioning standard.  As a result,
   programs should expect files in later minor versions to still match
   the markup from this document, but later major version /may/ not be
   backwards compatible.

   For example, version 1.1 might add another mandatory header
   parameter.  Code written for version 1.0 would still be able to
   read this header, though it would not be aware that the header had
   been mandatory.  Similarly, version 2.0 might add a comment syntax
   to the file format.  Code written for version 1.0 would not be
   expected to be capable of parsing these comments.

** Units

   By convention, parameters whose names end in =_unit= contain the
   measurement units of another parameter or column header.  The
   prefix to =_unit= must be the name of the parameter or column
   header that receives this unit.  For example, the parameter
   =Thickness_unit= gives units for the measurement in the parameter
   =Thickness=, while =Wavelength_unit= gives the units for all of the
   measurements in the =Wavelength= column of the body.

   These units are a space separated list of the individual
   coefficients in the measurement.  Each term begins with the unit
   name, which must be a string of letters, followed by an optional
   integer, representing the power.  For example, the unit of a Joule
   could be represented as =J= or as =kg m2 s-2=

** Thickness

   Every file *must* contain a thickness parameter and corresponding
   thickness unit.

** Theta_ymax and Theta_zmax

   The parameters =Theta_ymax= and =Theta_zmax= *must* be in every
   file.  These measurements represent the maximum scattering angle
   along the Y and Z directions accessible be the detector.  The
   corresponding units should only either be =radians= or =degrees=.

* Body

The end of the header is marked by a line containing =BEGIN_DATA=.
The body consists of a one line list of column names followed by the
data points, each on their own individual line.  Each data point
should have the same number of columns as the column names.

The column names cannot contain spaces.  There are four mandatory
column names, though other columns are permissible and any parser
*MUST* parse them, though the program is not required to use the other
values.  The column may occur in any order, though it is recommended
that the first four columns be =SpinEchoLength=, =Depolarisation=,
=Depolarisation_error=, and =Wavelength=, in order to simplify
graphing in utilities expecting three column ASCII.

** Error columns

   Any column whose name ends in =_error= is a measurement of the
   uncertainty of the column whose name was the prefix to =_error=.

** Spin Echo Length

This is the spin echo length probed by the data point.

** Depolarisation

   The depolarisation is the $\frac{\log P}{\lambda^2t}$ measured for
   this data point, where $t$ is the sample thickness, $\lambda$ is
   the wavelength of the neutrons used, and $P$ is the instrument
   normalised polarisation measured.

* Full Grammar

The following code is an Extended BNF grammar for the SES file format.
This grammar has been tested with python and haskell implementations.
The exact format of the grammar below is that used by the Tatsu parser
library, but the general grammar should be trivially convertible to
any other parser generator.

#+BEGIN_SRC text
@@whitespace :: /^\b$/

columnHeader = /[^\s]+/;
header = {@:columnHeader [lineSpace]}+ newline;

double = /[+-]?\d+\.?\d+([eE][+-]?\d+)?/;

lineSpace = /[ \t]+/;

newline = "\r\n" | "\n";

version = "FileFormatVersion" lineSpace @:double newline;

body = {@:bodyline}+;

bodyline = {@:bodyterm}+ newline;

bodyterm = {lineSpace}* @:double;

params = {(@+:orientation | @+:paramUnit | @+:paramDouble | @+:rawparam | lineSpace) newline}+;

orientation = "Orientation" lineSpace @:/[YZ]/;

paramDouble = @+:paramName lineSpace @+:double;

paramUnit = @+:paramNameUnit lineSpace {@+:unit [lineSpace]}+;

paramNameUnit = /.*_unit/;

unit = unitName [integer];

unitName = /[A-za-z]+/;

integer = /[+-]?\d+/;

rawparam = [@:paramLine];

paramLine = @+:paramName lineSpace @+:paramValue;

paramName = /\S+/;

paramValue = /[^\n]+/;

ses_file = version params "BEGIN_DATA\n" header body;
#+END_SRC
