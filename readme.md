Introduction
============

The `sestool` program provides a suite of utilities for managing SESANS
data.  This repository also includes the code library that was used to
write `sestool`.

Installation
------------

Binaries for Windows and Linux will be provided for each
[release](https://gitlab.com/rprospero/sesparser/releases).  If you
would like to compile the executable for yourself, or you're on an
unsupported platform (e.g. Mac OS), the process is as follows.

1) Install the [Haskell platform](https://www.haskell.org/platform/)
3) Download this source code
4) Compile the program via `cabal new-build sestool`
5) Optionally, test the code via `cabal new-test`

Examples
--------

	sestool -o final.ses combine low.ses high.ses

Combine two spin echo files into one file and store the result in
final.ses

	sestool filter Wavelength --low 4.0 polar_bear.ses

Remove all of the data points with a wavelength below 4 Angstroms and
print the updated file to the screen

	sestool rescale 10.0 -c Wavelength AT.ses BT.ses CT.ses DT.ses

Update all of the files in place with the Wavelength column and
associated uncertainty being 10 times it's former values (e.g. to
convert from nanometers to wavelength)

Usage
-----

sestool starts by taking a subcommand, followed by a list of options,
and finally a list of files.  The `-o` option can be used to set an
output file, otherwise the results will just be printed to the
terminal.  If more than one file would be created (e.g. filtering
multiple files), then each file will be updated inplace instead.

### combine

This subcommand requires no arguments.  It takes a list of files and
combines them into a single spin echo file.  When the file headers are
combined, the value from the earlier header is given preference.  The
final data points are sorted by spin echo length.

### down-convert

This subcommand requires no arguments.  It attempts to convert a file
from version 1.0 of the file format into the older version used before
the format was specified.

### filter

Filter requires a column name and optional takes a lower or upper
bound.  All data points within the file outside of these bounds are
excluded.  If no bounds are given, then the original file is returned.

### rescale

rescale requires a column name and a scale factor.  All of the values
in this column (and its corresponding uncertainty, if the column
exists) are multiplied by the given constant.
