{ pkgs ? import <nixpkgs> {} }:
with pkgs;

# Insert some useful and helpful comments explaining what the expressions do
stdenv.mkDerivation rec {
  name = "env";
  buildInputs = [ (pkgs.haskell.packages.ghc844.ghcWithPackages (p: [p.hlint p.lens p.megaparsec p.quickcheck-instances p.tasty p.tasty-hunit p.tasty-quickcheck]))
  cabal-install
                ];
}
