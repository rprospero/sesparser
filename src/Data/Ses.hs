{-|
Module      : Data.Ses
Description : Handling SES formatted SESANS data files
Copyright   : © Adam Washington, 2019
License     : BSD3
Maintainer  : adam.washington@stfc.ac.uk
Stability   : experimental

This module contains code for read and writing SES formatted SESANS
data, as well as a series of lenses and prisms for manipulating the
data once it has been read.
-}
module Data.Ses (module Data.Ses.Types, module Data.Ses.Parser, module Data.Ses.Writer) where

import Data.Ses.Types
import Data.Ses.Parser
import Data.Ses.Writer
