{-# LANGUAGE TypeFamilies #-}

{-|
Module      : Data.Ses.Parser
Description : Parse SESANS data from SES file
Copyright   : © Adam Washington, 2019
License     : BSD3
Maintainer  : adam.washington@stfc.ac.uk
Stability   : experimental

This module contains a Parser that will interpret an SES file into a SesData value.
-}

module Data.Ses.Parser (ses) where

import Control.Applicative ((<|>))
import Control.Lens
import Control.Monad (guard, unless, forM_, void)
import Control.Monad.Fail
import Control.Monad.State.Strict (get, modify, MonadState, runState, runStateT)
import Control.Monad.Trans.Class (lift)
import Control.Monad.Trans.Maybe
import Data.Char (isSpace, isLetter)
import Data.List (transpose)
import Data.Map.Strict as M (Map, fromList, assocs, delete, keys)
import Data.Ses.Types
import Data.Text (Text, unpack, pack, strip)
import Data.Void
import Text.Megaparsec as P
import Text.Megaparsec.Char as PC
import Text.Megaparsec.Char.Lexer as PL
import Prelude hiding (fail)

type Parser = Parsec Void Text

skipSpace :: Parser ()
skipSpace = PL.space PC.space1 empty empty

-- | Whitespace that will not induce line breaks
lineSpace :: Char -> Bool
lineSpace x = x == ' ' || x == '\t'

paramOrientation :: Parser (Text, HeaderTerm)
paramOrientation =
  (,) "Orientation" . Orientation <$>  ("Orientation" *> skipSpace *> letterChar)

paramUnit :: Parser (Text, HeaderTerm)
paramUnit = do
  name <- try $ manyTill (satisfy $ not . lineSpace) "_unit"
  skipSpace
  value <- P.sepBy ((,) <$> P.takeWhileP (Just "unitName") isLetter <*> option 1 (signed (pure ()) decimal)) $ P.takeWhile1P Nothing lineSpace
  return (pack name <> "_unit", Unit (pack name) $ Units value)

paramDouble :: Parser (Text, HeaderTerm)
paramDouble = do
  name <- P.takeWhile1P (Just "DoubleName") (not . lineSpace)
  skipSpace
  value <- nanDouble
  _ <- eol
  return (name, Measure $ Measurement value $ Units [])

paramRaw :: Parser (Text, HeaderTerm)
paramRaw = do
  name <- P.takeWhile1P (Just "RawHeaderName") (not . isSpace)
  () <- skipSpace
  value <- P.takeWhileP (Just "RawHeaderValue") (/= '\n')
  _ <- eol
  return (name, Raw $ strip value)

paramLine :: Parser (Text, HeaderTerm)
paramLine = (
  (paramOrientation <?> "ParamOrientation") <|>
  (paramUnit <?> "ParamUnit") <|>
  (try paramDouble  <?> "ParamDouble") <|>
  (paramRaw <?> "ParamRaw")) <* skipSpace

headerName :: Parser Text
headerName = P.takeWhile1P (Just "ColumnHeader") (not . isSpace)
headerLine :: Parser [Text]
headerLine = P.sepBy headerName
  $ P.takeWhile1P (Just "ColumnHeaderLine") lineSpace

nanDouble :: Parser Double
nanDouble = try ("nan" >> return (0/0))
  <|> try (signed (pure ()) float)
  <|> (fromIntegral <$> (decimal :: Parser Integer))

bodyLine :: Parser [Double]
bodyLine = P.sepBy nanDouble $ P.takeWhile1P (Just "bodyLine") lineSpace

beginData :: Parser Text
beginData = "BEGIN_DATA" <* eol

sesHeaderP :: Parser SesHeader
sesHeaderP = mergeHeaders . fromList <$> manyTill paramLine beginData

-- | Parse SESANS data from an SES formatted file
ses :: Parser SesData
ses = do
  version <- "FileFormatVersion" *> skipSpace *> float <* skipSpace
  unless (version >= 1) $ fail "Version is too old"
  unless (version < 2 && version >= 1) $ fail $ "Version " <> show version <> " not yet supported"
  params' <- sesHeaderP
  ([Measure thick, Measure thy, Measure thz, Orientation c],
   params) <- flip runStateT params' $ sequence [
    insistHeader "Thickness"
    , insistHeader "Theta_ymax"
    , insistHeader "Theta_zmax"
    , insistHeader "Orientation"]
  header <- headerLine <* eol
  body <- P.sepBy bodyLine eol <* eof
  unless (length header == length (transpose body)) $ fail "Unequal number of headers and columns"
  let cols = fromList . zip header . map (SesColumn Nothing Nothing) . transpose $ body
  ([sel, depol, wavelength], (final_params, final_body)) <-
    flip runStateT (params, cols) $ do
        mergeColumns
        sequence [insist' "SpinEchoLength"
                 , insist' "Depolarisation"
                 , insist' "Wavelength"]
  return SesData {
    sesVersion = version,
    sesOrientation = c,
    sesThickness = thick,
    sesThetaY = thy,
    sesThetaZ = thz,
    sesHeader = clean final_body final_params,
    sesColumns = final_body,
    sesEchoLength = sel,
    sesDepolarisation = depol,
    sesWavelength = wavelength
    }

clean :: Map Text SesColumn -> SesHeader -> SesHeader
clean body params = foldl go params $ assocs body
  where
    go h (name, _) = delete (name <> "_unit") h

mandate :: (MonadFail m) => String -> m (Maybe a) -> m a
mandate notice mx = do
  x <- mx
  case x of
    Nothing -> fail notice
    Just x' -> return x'

insistHeader :: (MonadFail m, MonadState SesHeader m) => Text -> m HeaderTerm
insistHeader key = do
  mandate ("Missing mandatory header " <> unpack key) $ at key <<.= Nothing


insist' :: (MonadFail m, MonadState (SesHeader, SesBody) m) => Text -> m SesColumn
insist' key = do
  x <- mandate ("Column " <> unpack key <> " is missing from the data") $
    _2 . at key <<.= Nothing
  _ <- mandate ("No units for column " <> unpack key) $
    _1 . at (key <> "_unit") <<.= Nothing
  return x

mergeColumns :: (MonadFail m, MonadState (SesHeader, SesBody) m) => m ()
mergeColumns = do
  (_, st) <- get
  forM_ (keys st) $ \key -> do
    (h, new) <- get
    let errs = new ^? at (key <> "_error") . _Just . _sesColValues
    _2 . at key . _Just . _sesColError .= errs
    _2 . at (key <> "_error") .= Nothing
    let u = h ^? at (key <> "_unit") . _Just . _Unit . _2
    _2 . at key . _Just . _sesColUnit .= u
    _1 . at (key <> "_error") .= Nothing

mergeHeaders :: SesHeader -> SesHeader
mergeHeaders params = snd . flip runState params $ forM_ (M.keys params) go
  where
    go key = do
      st <- get
      void $ runMaybeT $ do
          (new_key, unit) <- MaybeT . return $ st ^? at key . _Just ._Unit
          old_unit <- MaybeT . return $ st ^? at new_key . _Just . _Measure . _measurementUnit . _Units
          guard $ null old_unit
          at new_key  . _Just . _Measure . _measurementUnit .= unit
          lift $ modify $ sans key
