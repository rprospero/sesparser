{-# LANGUAGE DeriveGeneric, TypeOperators #-}

{-|
Module      : Data.Ses.Types
Description : Data types for holding SESANS experiment data
Copyright   : © Adam Washington, 2019
License     : BSD3
Maintainer  : adam.washington@stfc.ac.uk
Stability   : experimental

This module contains the data types needed to contain a SESANS file in
the SES file format.  It also currently contains the Merge typeclass,
which may need to go into its own module, or be eliminated entirely.
-}

module Data.Ses.Types (SesData(..)
                      , _sesVersion
                      , _sesHeader
                      , _sesOrientation
                      , _sesThickness
                      , _sesThetaY
                      , _sesThetaZ
                      , _sesEchoLength
                      , _sesDepolarisation
                      , _sesWavelength
                      , _sesColumns
                      , SesColumn(..)
                      , _sesColUnit
                      , _sesColError
                      , _sesColValues
                      , Units(..)
                      , _Units
                      , HeaderTerm(..)
                      , _Unit
                      , _Measure
                      , _Raw
                      , SesHeader
                      , SesBody
                      , Measurement(..)
                      , _measurementUnit
                      , _measurementValue
                      ) where

import Control.Applicative ((<|>))
import Control.Lens (Lens', lens, prism', Prism')
import Data.Map (Map, keys, intersection, fromList, (!))
import Data.Text (Text)
import GHC.Generics

-- | The information in the initial header of an SES file
type SesHeader = Map Text HeaderTerm
-- | The numerical data in the columns of an SES file
type SesBody = Map Text SesColumn

-- | The physical units of a measurement, as a list of units and their exponents
newtype Units = Units [(Text, Int)]
  deriving (Show, Eq, Generic)
-- | A measurement with corresponding units
data Measurement = Measurement Double Units
  deriving (Show, Eq, Generic)

-- | A lens into the raw units of the Units type
_Units :: Lens' Units [(Text, Int)]
_Units = lens (\(Units x) -> x) (const Units)

-- | A lens into the numerical portion of a measurement
_measurementValue :: Lens' Measurement Double
_measurementValue = lens (\(Measurement x _) -> x) (\(Measurement _ u) x -> Measurement x u)
-- | A Lens into the units of a measurement
_measurementUnit :: Lens' Measurement Units
_measurementUnit = lens (\(Measurement _ x) -> x) (\(Measurement x _) u -> Measurement x u)

-- | A single header parameter from an SES file
data HeaderTerm = Raw Text -- ^ An uninterpreted text header
  | Measure Measurement -- ^ A measurement with units
  | Unit Text Units -- ^ A unit marker, possibly for a column or another header
  | Orientation Char -- ^ An orientation direction
  deriving (Show, Eq, Generic)

-- | A prism onto Raw HeaderTerms
_Raw :: Prism' HeaderTerm Text
_Raw = prism' Raw getter
  where
    getter (Raw x) = Just x
    getter _ = Nothing
-- | A prism onto Measure HeaderTerms
_Measure :: Prism' HeaderTerm Measurement
_Measure = prism' Measure getter
  where
    getter (Measure x) = Just x
    getter _ = Nothing
-- | A prism onto Orientation HeaderTerms
_Orientation :: Prism' HeaderTerm Char
_Orientation = prism' Orientation getter
  where
    getter (Orientation x) = Just x
    getter _ = Nothing
-- | A prism onto Unit HeaderTerms
_Unit :: Prism' HeaderTerm (Text, Units)
_Unit = prism' (uncurry Unit) getter
  where
    getter (Unit a b) = Just (a, b)
    getter _ = Nothing

-- | A column of numerical data from an ses file, including metadata
data SesColumn = SesColumn {
    sesColUnit :: Maybe Units, -- ^ The physical units of the values in the column
    sesColError :: Maybe [Double], -- ^ The corresponding uncertainty for each data point
    sesColValues :: [Double] -- ^ The numerical values of the data
  }
  deriving (Show, Eq, Generic)

-- | A lens into the units of a column
_sesColUnit :: Lens' SesColumn (Maybe Units)
_sesColUnit = lens sesColUnit (\column x -> column{sesColUnit=x})
-- | A lens into the uncertainty of a column
_sesColError :: Lens' SesColumn (Maybe [Double])
_sesColError = lens sesColError (\column x -> column{sesColError=x})
-- | A lens into the values in a column
_sesColValues :: Lens' SesColumn [Double]
_sesColValues = lens sesColValues (\column x -> column{sesColValues=x})

-- | The data from a SESANS experiment that was recorded in an SES formatted file.
data SesData = SesData {
  sesVersion :: Double, -- ^ The file format version of the data file
  sesHeader :: SesHeader, -- ^ The optional head data included in the file
  sesOrientation :: Char, -- ^ The orientation direction of the SESANS encoding
  sesThickness :: Measurement, -- ^ The thickness of the sample
  sesThetaY :: Measurement, -- ^ The maximum acceptance angle in the Y direction
  sesThetaZ :: Measurement, -- ^ The maximum acceptange angle in the Z direction
  sesEchoLength :: SesColumn, -- ^ The spin echo lengths of the data points
  sesDepolarisation :: SesColumn, -- ^ The measured depolarisation of
                                 -- the data points.  By the file
                                 -- format specification, this column
                                 -- is guaranteed to include its
                                 -- uncertainty values
  sesWavelength :: SesColumn, -- ^ The wavelength for each data point
  sesColumns :: Map Text SesColumn -- ^ The optional columns included in the data.
} deriving (Show, Eq, Generic)

-- | A lens into the version of an SES file
_sesVersion :: Lens' SesData Double
_sesVersion = lens sesVersion (\x v -> x{sesVersion=v})
-- | A lens into the header of an SES file
_sesHeader :: Lens' SesData SesHeader
_sesHeader = lens sesHeader (\x v -> x{sesHeader=v})
-- | A lens into the orientation of an SES file
_sesOrientation :: Lens' SesData Char
_sesOrientation = lens sesOrientation (\x v -> x{sesOrientation=v})
-- | A lens into the thickness of an SES file
_sesThickness :: Lens' SesData Measurement
_sesThickness = lens sesThickness (\x v -> x{sesThickness=v})
-- | A lens into the y theta acceptance of an SES file
_sesThetaY :: Lens' SesData Measurement
_sesThetaY = lens sesThetaY (\x v -> x{sesThetaY=v})
-- | A lens into the z theta acceptance of an SES file
_sesThetaZ :: Lens' SesData Measurement
_sesThetaZ = lens sesThetaZ (\x v -> x{sesThetaZ=v})
-- | A lens into the spin echo length column of an SES file
_sesEchoLength :: Lens' SesData SesColumn
_sesEchoLength = lens sesEchoLength (\x v -> x{sesEchoLength=v})
-- | A lens into the depolarisation column of an SES file
_sesDepolarisation :: Lens' SesData SesColumn
_sesDepolarisation = lens sesDepolarisation (\x v -> x{sesDepolarisation=v})
-- | A lens into the wavelength column of an SES file
_sesWavelength :: Lens' SesData SesColumn
_sesWavelength = lens sesWavelength (\x v -> x{sesWavelength=v})
-- | A lens into the optional columns of an SES file
_sesColumns :: Lens' SesData (Map Text SesColumn)
_sesColumns = lens sesColumns (\x v -> x{sesColumns=v})


instance Semigroup SesData where
  a <> b = a {sesColumns = mergeColumns (sesColumns a) $  sesColumns b
             , sesEchoLength = sesEchoLength a `mergeColumn` sesEchoLength b
             , sesDepolarisation = sesDepolarisation a `mergeColumn` sesDepolarisation b
             , sesWavelength = sesWavelength a `mergeColumn` sesWavelength b
             }

mergeColumns :: Map Text SesColumn -> Map Text SesColumn -> Map Text SesColumn
mergeColumns a b =
  let columns = keys $ intersection a b
  in
    fromList . flip map columns $ \x -> (x, mergeColumn (a ! x) (b ! x))

mergeColumn :: SesColumn -> SesColumn -> SesColumn
mergeColumn a b = SesColumn {
  sesColUnit = sesColUnit a <|> sesColUnit b,
  sesColError = do
      ma <- sesColError a
      mb <- sesColError b
      return $ ma <> mb,
  sesColValues = sesColValues a <> sesColValues b
  }
