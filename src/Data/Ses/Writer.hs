{-|
Module      : Data.Ses.Writer
Description : Format SesData into an SES formatted string
Copyright   : © Adam Washington, 2019
License     : BSD3
Maintainer  : adam.washington@stfc.ac.uk
Stability   : experimental

This module exports a function 'printSes' that will take the data in
an SesData value and compile it into the SES format for export.
-}

module Data.Ses.Writer (printSes, printSesOld) where

import Control.Lens
import Data.Function (on)
import Data.List (transpose, sortBy)
import Data.Map.Strict (Map, insert, assocs, foldMapWithKey)
import Data.Ses.Types
import Data.Text as T hiding (map)

showT :: (Show x) => x -> Text
showT = T.pack . show

printUnit :: Units -> Text
printUnit (Units xs) = T.unwords . map go $ xs
  where
    -- go (_, 0) = ""
    go (name, 1) = name
    go (name, power) = name <> showT power

printColumnUnits :: Text -> SesColumn -> [Text]
printColumnUnits name column = case sesColUnit column of
  Nothing -> []
  Just x -> [name <> "_unit " <> printUnit x]

printColumns :: Map Text SesColumn -> [Text]
printColumns = colsPrint . sortBy (on colSort fst)  . foldMapWithKey go
  where
    go :: Text -> SesColumn -> [(Text, [Double])]
    go name column = case sesColError column of
        Nothing -> [(name, sesColValues column)]
        Just err -> [(name, sesColValues column), (name <> "_error", err)]
    colSort "SpinEchoLength" _ = LT
    colSort _ "SpinEchoLength" = GT
    colSort "Depolarisation" _ = LT
    colSort _ "Depolarisation" = GT
    colSort "Depolarisation_error" _ = LT
    colSort _ "Depolarisation_error" = GT
    colSort "Wavelength" _ = LT
    colSort _ "Wavelength" = GT
    colSort x y = x `compare` y
    colsPrint :: [(Text, [Double])] -> [Text]
    colsPrint xs = [T.unwords $ map fst xs] <> (map (T.unwords . map showT) . sortBy (on compare Prelude.head) $ Data.List.transpose (map snd xs))


printHeader :: Text -> HeaderTerm -> [Text]
printHeader name (Raw x) = [name <> " " <> x]
printHeader name (Orientation c) = [name <> " " <> pack [c]]
printHeader name (Unit _ u) = [name <> "_unit " <> printUnit u]
printHeader name (Measure (Measurement x (Units []))) = [name <> " " <> showT x]
printHeader name (Measure (Measurement x u)) = [name <> " " <> showT x, name <> "_unit " <> printUnit u]

-- | Compile SESANS data into an SES formatted file.  The attempt is to ensure that
--
-- > parseOnly ses . printSes = Right
printSes :: SesData -> Text
printSes x = T.unlines $ [
  "FileFormatVersion " <> showT (sesVersion x)
  , "Orientation " <> pack [sesOrientation x]]
  <> printHeader "Thickness" (Measure $ sesThickness x)
  <> printHeader "Theta_ymax" (Measure $ sesThetaY x)
  <> printHeader "Theta_zmax" (Measure $ sesThetaZ x)
  <> printColumnUnits "SpinEchoLength" (sesEchoLength x)
  <> printColumnUnits "Depolarisation" (sesDepolarisation x)
  <> printColumnUnits "Wavelength" (sesWavelength x)
  <> foldMapWithKey printHeader (sesHeader x)
  <> Prelude.concatMap (uncurry printColumnUnits) (assocs (sesColumns x))
  <> ["" , "BEGIN_DATA"]
  <> printColumns allColumns
  where
    allColumns = insert "SpinEchoLength" (sesEchoLength x) $ insert "Depolarisation" (sesDepolarisation x) $ insert "Wavelength" (sesWavelength x) $ sesColumns x

----  The code for writing the old format


-- | Compile SESANS data into an older format from before the
-- standardization.
printSesOld :: SesData -> Text
printSesOld x = T.unlines $
  foldMapWithKey printOldHeader (sesHeader x)
  <> printOldHeader "Thickness" (Measure $ sesThickness x)
  <> printOldHeader "Theta_ymax" (Measure $ sesThetaY x)
  <> printOldHeader "Theta_zmax" (Measure $ sesThetaZ x)
  <> [""]
  <> printOldColumns allColumns
  where
    allColumns = insert "SpinEchoLength" (sesEchoLength x) $ insert "Depolarisation" (sesDepolarisation x) $ insert "Wavelength" (sesWavelength x) $ sesColumns x

printOldHeader :: Text -> HeaderTerm -> [Text]
printOldHeader name (Raw x) = [name <> "\t" <> x]
printOldHeader name (Orientation x) = [name <> "\t" <> pack [x]]
printOldHeader _    (Unit _ _) = []
printOldHeader name (Measure (Measurement x (Units []))) =
  [name <> "\t" <> showT x]
printOldHeader name (Measure (Measurement x u)) =
  [name <> " [" <> printOldUnits u <> "]\t" <> showT x]

printOldUnits :: Units -> Text
printOldUnits (Units u) =
  T.unwords . map go $ u
  where
    go (_, 0) = ""
    go (x, 1) = x
    go (x, n) = x <> "^" <> showT n

printOldColumns :: Map Text SesColumn -> [Text]
printOldColumns = colsPrint . sortBy (on colSort (^. _1))  . foldMapWithKey go
  where
    go :: Text -> SesColumn -> [(Text, Maybe Units, [Double])]
    go name column = case sesColError column of
        Nothing -> [(name, sesColUnit column, sesColValues column)]
        Just err -> [(name, sesColUnit column, sesColValues column), (name <> "_error", Nothing, err)]
    colSort "SpinEchoLength" _ = LT
    colSort _ "SpinEchoLength" = GT
    colSort "SpinEchoLength_error" _ = LT
    colSort _ "SpinEchoLength_error" = GT
    colSort "Wavelength" _ = LT
    colSort _ "Wavelength" = GT
    colSort "Wavelength_error" _ = LT
    colSort _ "Wavelength_error" = GT
    colSort "Depolarisation" _ = LT
    colSort _ "Depolarisation" = GT
    colSort "Depolarisation_error" _ = LT
    colSort _ "Depolarisation_error" = GT
    colSort x y = x `compare` y
    colsPrint :: [(Text, Maybe Units, [Double])] -> [Text]
    colsPrint xs = [T.intercalate "\t" . map makeHeader $ xs] <> (map (T.intercalate "\t" . map showT) . sortBy (on compare Prelude.head) $ Data.List.transpose (map (^. _3) xs))
    makeHeader (name, Nothing, _) = oldName name
    makeHeader (name, Just u, _) = oldName name <> " [" <> printOldUnits u <> "]"
    oldName "SpinEchoLength" = "spin echo length"
    oldName "SpinEchoLength_error" = "error SEL"
    oldName x =
      case T.stripSuffix "_error" x of
        Nothing -> toLower x
        Just base -> "error " <> oldName base
